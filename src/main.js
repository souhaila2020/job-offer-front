import { createApp } from 'vue';
import App from './App.vue';
// import Vuetify from 'vuetify';
// import 'vuetify/dist/vuetify.css';
import store from './store';
import router from './router'; 

const app = createApp(App);

// import vuetify from './plugins/vuetify';
// app.use(vuetify);

app.use(store);
app.use(router); // Use the router

app.mount('#app');
