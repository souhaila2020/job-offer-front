import api from '@/services/api';

const state = {
  jobOffers: [],
};

const mutations = {
  SET_JOB_OFFERS(state, jobOffers) {
    state.jobOffers = jobOffers;
  },
};

const actions = {
  async fetchJobOffers({ commit }) {
    try {
      const response = await api.get('/job-offers');
      commit('SET_JOB_OFFERS', response.data.data);
    } catch (error) {
      console.error('Error fetching job offers:', error);
      throw error;
    }
  },

  async applyForJob({ dispatch }, jobId, formData) {
    try {
      console.log("Request FormData:", formData);

      const response = await api.post(
        `/users/apply-for-job/${jobId}`,
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );

      console.log("Response Data:", response.data);

      dispatch("fetchJobOffers");
      return response;
    } catch (error) {
      console.error("Error applying for job:", error);
      throw error;
    }
  },
};

const getters = {
  jobOffers: (state) => state.jobOffers,
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
