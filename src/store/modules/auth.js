import api from '@/services/api';

const state = {
  user: null,
  token: null,
};

const mutations = {
  SET_USER(state, user) {
    state.user = user;
  },
  SET_TOKEN(state, token) {
    state.token = token;
  },
};

const actions = {
  async register({ commit }, userData) {
    try {
      const response = await api.post('/auth/register', userData);
      commit('SET_USER', response.data.user);
      commit('SET_TOKEN', response.data.token);
      return response.data;
    } catch (error) {
      console.error('Registration error:', error.response);
      throw error.response.data;
    }
  },

  async login({ commit }, userData) {
    try {
      const response = await api.post('/auth/login', userData);
      commit('SET_USER', response.data.user);
      commit('SET_TOKEN', response.data.token);
      return response.data;
    } catch (error) {
      console.error('Login error:', error.response);
      throw error.response.data;
    }
  },
};

export default {
  state,
  mutations,
  actions,
};
