import api from "@/services/api";

const state = {
  resumes: [],
};

const mutations = {
  SET_RESUMES(state, resumes) {
    state.resumes = resumes;
  },
};

const actions = {
  async fetchResumes({ commit }) {
    try {
      const response = await api.get("/resumes");
      commit("SET_RESUMES", response.data);
    } catch (error) {
      console.error("Error fetching resumes:", error);
    }
  },

  async createOrUpdateResume({ dispatch }, formData) {
    try {
      let response;

      if (formData.id) {
        // Update existing CV
        response = await api.put(`/resumes/${formData.id}`, formData);
      } else {
        // Create new CV
        response = await api.post("/resumes", formData);
      }

      // Fetch updated list of resumes after creating/updating
      dispatch("fetchResumes");

      response.data.pdf_path = "http://localhost:8000" + response.data.pdf_path;

      // Return the response data
      return response.data;
    } catch (error) {
      console.error("Error creating/updating resume:", error);
      throw error;
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
