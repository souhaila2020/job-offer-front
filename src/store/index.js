import { createStore } from 'vuex';
import auth from './modules/auth';
import resume from './modules/resume';
import jobOffer from './modules/jobOffer';

const store = createStore({
  modules: {
    auth,
    resume,
    jobOffer,
  },
});

export default store;
