import { createRouter, createWebHistory } from 'vue-router';
import HelloWorld from './components/HelloWorld.vue'; 
import Register from './components/Register.vue'; 
import Login from './components/Login.vue';
import ResumeForm from './components/ResumeForm.vue';
import JobOfferList from './components/JobOfferList.vue';
import ApplyForJob from './components/ApplyForJob.vue';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: HelloWorld,
  },
  {
    path: '/register',
    name: 'Register',
    component: Register,
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
  },
  {
    path: '/resumes/create',
    component: ResumeForm,
  },
  {
    path: '/resumes/:id/edit',
    component: ResumeForm,
  },
  {
    path: '/job-offers',
    name: 'JobOfferList',
    component: JobOfferList,
  },
  {
    path: '/apply-for-job/:id',
    name: 'ApplyForJob',
    component: ApplyForJob,
  },
  {
    path: '/job-offers',
    name: 'JobOfferList',
    component: JobOfferList,
  },
  {
    path: '/apply-for-job/:id',
    name: 'ApplyForJob',
    component: ApplyForJob,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
